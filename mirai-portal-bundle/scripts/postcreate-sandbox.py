#!/usr/bin/python3.4

import sys
import json
import os

def app_create_config(app_config):
    app_ns = app_config['app_ns']
    os.system("kubectl delete vs leap-vs -n {}".format(app_ns))
    os.system("kubectl create -f leap-vs-sandbox.yaml -n {}".format(app_ns))
    
def create_service(app_config):
    appname = app_config['name'] 
    appns = app_config['app_ns']

    for r in app_config.get('roles', []):
        if r.get('name') in ("portal"):
            svc_labels = r.get('vnodes')[0]['env']['allocated']['SVC_LABELS'].replace(',',' ')
            os.system("kubectl label svc -n {} {}-{}-lb-0 {}".format(appns, appname, r.get('name'), svc_labels))

def main():
    json_file = sys.argv[1]
    with open(json_file) as f:
        app_config = json.load(f)
    app_create_config(app_config)
    #create_service(app_config)

if __name__ == "__main__":
    main()
