#!/usr/bin/python3.4

import sys
import json
import time
import tempfile
import os

def delete_virtual_service(app_config):
    appns = app_config['app_ns']
    os.system("kubectl delete vs leap-vs -n {}".format(appns))

def main():
    json_file = sys.argv[1]
    with open(json_file) as f:
        app_config = json.load(f)
    delete_virtual_service(app_config)

if __name__ == "__main__":
    main()