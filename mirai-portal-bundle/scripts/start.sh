#!/bin/bash

sleep 10
unset HTTPS_PROXY
unset HTTP_PROXY

export VAULT_ADDR="https://[240b:c0e0:101:5476:1c01:2:0:5101]:8200" 

export JWT=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token) 
echo $JWT

#Sandbox 3F
export CLUSTERNAME=uhn7klr22
export VAULT_ROLE=leap-role

export VAULT_TOKEN=$(curl -kg -X POST -d '{"jwt":"'$JWT'","role":"'$VAULT_ROLE'"}' $VAULT_ADDR/v1/rcp/auth/$CLUSTERNAME/login | jq -r ".auth.client_token")
echo $VAULT_TOKEN

export MONGO_USERNAME=$(curl -kg -H "X-Vault-Token: $VAULT_TOKEN" $VAULT_ADDR/v1/rcp/kv/data/$CLUSTERNAME/leap/mongo/username | jq -r ".data.data.secret")

export MONGO_PASSWORD=$(curl -kg -H "X-Vault-Token: $VAULT_TOKEN" $VAULT_ADDR/v1/rcp/kv/data/$CLUSTERNAME/leap/mongo/password | jq -r ".data.data.secret")

export QUE_USERNAME=$(curl -kg -H "X-Vault-Token: $VAULT_TOKEN" $VAULT_ADDR/v1/rcp/kv/data/$CLUSTERNAME/leap/rabbitmq/username | jq -r ".data.data.secret")

export QUE_PASSWORD=$(curl -kg -H "X-Vault-Token: $VAULT_TOKEN" $VAULT_ADDR/v1/rcp/kv/data/$CLUSTERNAME/leap/rabbitmq/password | jq -r ".data.data.secret")

pm2-docker index.js
